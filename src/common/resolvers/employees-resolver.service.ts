import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IEmployeeModel } from 'src/app/store/state/employee.state';
import { IAppState } from 'src/app/store/state/app.state';
import { EmployeeService } from 'src/app/employee/employee.service';
import { Store } from '@ngrx/store';
import { Resolve } from '@angular/router';
import { getAllEmployees } from 'src/app/store/selectors/employee.selectors';

@Injectable()
export class EmployeesResolver implements Resolve<any> {
  employees: Observable<IEmployeeModel[]>;
  constructor(
    private readonly store: Store<IAppState>,
    private readonly service: EmployeeService,
  ) {}
  resolve() {
    const employees = this.store.select(getAllEmployees);
    if (employees === undefined) {
      return false;
    } else {
      return this.service.getAllEmployees();
    }
  }
}
