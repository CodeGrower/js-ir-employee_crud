import { IEmployeeState, initialIEmployeeState } from './employee.state';

export interface IAppState {
  employeeState: IEmployeeState;
}

export const initialAppState = {
  employeeState: initialIEmployeeState,
};

export function getInitialAppState(): IAppState {
  return initialAppState;
}
