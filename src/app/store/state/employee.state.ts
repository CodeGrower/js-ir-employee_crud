export interface IEmployeeModel {
  id: string;
  employee_name: string;
  employee_salary: string;
  employee_age: string;
  profile_image: string;
}

export interface IEmployeeState {
  employee: IEmployeeModel;
  all_employees: IEmployeeModel[];
}

export const initialIEmployeeState = {
  employee: null,
  all_employees: [],
};

