import { IAppState } from '../state/app.state';
import { IEmployeeState } from '../state/employee.state';
import { createSelector } from '@ngrx/store';

const selectEmployee = (state: IAppState) => state.employeeState;

export const getEmployee = createSelector(selectEmployee, (state: IEmployeeState) => state.employee);
export const getAllEmployees = createSelector(selectEmployee, (state: IEmployeeState) => state.all_employees);
