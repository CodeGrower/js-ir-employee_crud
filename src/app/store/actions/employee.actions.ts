import { Action } from '@ngrx/store';

import { IEmployeeModel } from '../state/employee.state';

export enum EEmployeeActions {
  GetEmployees = '[Employees] Get Employees',
  GetEmployeesSuccess = '[Employees] Get Employees Success',
  GetEmployeesFail = '[Employees] Get Employees Fail',
  GetEmployee = '[Employees] Get Employee',
  GetEmployeeSuccess = '[Employees] Get Employee Success',
  GetEmployeeFail = '[Employees] Get Employee Fail',
  CreateEmployee = '[Employees] Create Employee',
  CreateEmployeeSuccess = '[Employees] Create Employee Success',
  CreateEmployeeFail = '[Employees] Create Employee Fail',
  UpdateEmployee = '[Employees] Update Employee',
  UpdateEmployeeSuccess = '[Employees] Update Employee Success',
  UpdateEmployeeFail = '[Employees] Update Employee Fail',
  DeleteEmployee = '[Employees] Delete Employee',
  DeleteEmployeeSuccess = '[Employees] Delete Employee Success',
  DeleteEmployeeFail = '[Employees] Delete Employee Fail',
}

export class GetEmployees implements Action {
  public readonly type = EEmployeeActions.GetEmployees;
}

export class GetEmployeesSuccess implements Action {
  public readonly type = EEmployeeActions.GetEmployeesSuccess;

  constructor(public payload: IEmployeeModel[]) {}
}

export class GetEmployeesFail implements Action {
  public readonly type = EEmployeeActions.GetEmployeesFail;
}

export class GetEmployee implements Action {
  public readonly type = EEmployeeActions.GetEmployee;

  constructor(public payload: string) {}
}

export class GetEmployeeSuccess implements Action {
  public readonly type = EEmployeeActions.GetEmployeeSuccess;

  constructor(public payload: IEmployeeModel) {}
}

export class GetEmployeeFail implements Action {
  public readonly type = EEmployeeActions.GetEmployeeFail;
}

export class CreateEmployee implements Action {
  public readonly type = EEmployeeActions.CreateEmployee;

  constructor(public payload: IEmployeeModel) {}
}

export class CreateEmployeeSuccess implements Action {
  public readonly type = EEmployeeActions.CreateEmployeeSuccess;

  constructor(public payload: IEmployeeModel) {}
}

export class CreateEmployeeFail implements Action {
  public readonly type = EEmployeeActions.CreateEmployeeFail;
}

export class UpdateEmployee implements Action {
  public readonly type = EEmployeeActions.UpdateEmployee;

  constructor(public payload: IEmployeeModel, public employeeID: number) {}
}

export class UpdateEmployeeSuccess implements Action {
  public readonly type = EEmployeeActions.UpdateEmployeeSuccess;

  constructor(public payload: IEmployeeModel) {}
}

export class UpdateEmployeeFail implements Action {
  public readonly type = EEmployeeActions.UpdateEmployeeFail;
}

export class DeleteEmployee implements Action {
  public readonly type = EEmployeeActions.DeleteEmployee;

  constructor(public payload: string) {}
}

export class DeleteEmployeeSuccess implements Action {
  public readonly type = EEmployeeActions.DeleteEmployeeSuccess;

  constructor(public payload: IEmployeeModel) {}
}

export class DeleteEmployeeFail implements Action {
  public readonly type = EEmployeeActions.DeleteEmployeeFail;
}

export type EmployeeActions =
GetEmployees  |
GetEmployeesSuccess |
GetEmployeesFail |
GetEmployee |
GetEmployeeSuccess  |
GetEmployeeFail |
CreateEmployee  |
CreateEmployeeSuccess |
CreateEmployeeFail  |
UpdateEmployee  |
UpdateEmployeeSuccess |
UpdateEmployeeFail  |
DeleteEmployee  |
DeleteEmployeeSuccess |
DeleteEmployeeFail;
