import { Injectable } from '@angular/core';
import { Effect, ofType, Actions } from '@ngrx/effects';
import { switchMap, catchError, map } from 'rxjs/operators';
import { of } from 'rxjs';

import {
  EEmployeeActions,
  GetEmployeesSuccess,
  GetEmployeeSuccess,
  CreateEmployeeSuccess,
  UpdateEmployeeSuccess,
  DeleteEmployeeSuccess,
  GetEmployeesFail,
  GetEmployeeFail,
  CreateEmployeeFail,
  UpdateEmployeeFail,
  DeleteEmployeeFail,
} from '../actions/employee.actions';
import { EmployeeService } from 'src/app/employee/employee.service';
import { IEmployeeModel } from '../state/employee.state';

@Injectable()
export class EmployeeEffects {
  @Effect()
  getEmployees = this.actions$.pipe(
    ofType(EEmployeeActions.GetEmployees),
    switchMap(() => {
      return this.employeeService.getAllEmployees().pipe(
        map((response: IEmployeeModel[]) => {
          return new GetEmployeesSuccess(response);
        })
      ),
      catchError(() => of(new GetEmployeesFail()));
    })
  );

  @Effect()
  getEmployee = this.actions$.pipe(
    ofType(EEmployeeActions.GetEmployee),
    switchMap((employeeData: string) => {
      const employeeID = Number(employeeData);
      return this.employeeService.getEmployee(employeeID).pipe(
        map((response: IEmployeeModel) => {
          return new GetEmployeeSuccess(response);
        })
      ),
      catchError(() => of(new GetEmployeeFail()));
    })
  );

  @Effect()
  createEmployee = this.actions$.pipe(
    ofType(EEmployeeActions.CreateEmployee),
    switchMap((employeeData: IEmployeeModel) => {
      return this.employeeService.createEmployee(employeeData).pipe(
        map((response: IEmployeeModel) => {
          return new CreateEmployeeSuccess(response);
        })
      ),
      catchError(() => of(new CreateEmployeeFail()));
    })
  );

  @Effect()
  updateEmployee = this.actions$.pipe(
    ofType(EEmployeeActions.UpdateEmployee),
    switchMap((employeeData: IEmployeeModel, employeeID: number) => {
      return this.employeeService.updateEmployee(employeeID, employeeData).pipe(
        map((response) => {
          return new UpdateEmployeeSuccess(response);
        })
      ),
      catchError(() => of(new UpdateEmployeeFail()));
    })
  );

  @Effect()
  deleteEmployee = this.actions$.pipe(
    ofType(EEmployeeActions.DeleteEmployee),
    switchMap((ID: string) => {
      const employeeID = Number(ID);
      return this.employeeService.deleteEmployee(employeeID).pipe(
        map((response: IEmployeeModel) => {
          return new DeleteEmployeeSuccess(response);
        })
      ),
      catchError(() => of(new DeleteEmployeeFail()));
    })
  );

  constructor(
    private actions$: Actions,
    private employeeService: EmployeeService,
  ) {}
}
