import { IAppState } from '../state/app.state';
import { ActionReducerMap } from '@ngrx/store';

import { employeeReducer } from './employee.reducers';


export const appReducers: ActionReducerMap<IAppState, any> = {
  employeeState: employeeReducer,
};
