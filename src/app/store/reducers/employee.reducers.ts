import { initialIEmployeeState, IEmployeeState } from '../state/employee.state';
import { EmployeeActions, EEmployeeActions } from '../actions/employee.actions';

export const employeeReducer = (
  state = initialIEmployeeState,
  action: EmployeeActions
): IEmployeeState => {
  switch (action.type) {
    case EEmployeeActions.GetEmployeeSuccess: {
      return {
        ...state,
        employee: action.payload
      };
    }
    case EEmployeeActions.GetEmployeesSuccess: {
      return {
        ...state,
        all_employees: action.payload
      };
    }
    case EEmployeeActions.UpdateEmployeeSuccess: {
      return {
        ...state,
        employee: action.payload
      };
    }
    case EEmployeeActions.CreateEmployeeSuccess: {
      return {
        ...state,
        employee: action.payload
      };
    }
    case EEmployeeActions.DeleteEmployeeSuccess: {
      return {
        ...state,
        employee: action.payload
      };
    }
  }
};

