import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { IEmployeeModel } from 'src/app/store/state/employee.state';
import { IAppState } from 'src/app/store/state/app.state';
import { Store } from '@ngrx/store';
import { UpdateEmployee } from 'src/app/store/actions/employee.actions';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {
  allEmployees: IEmployeeModel[];
  singleEmployee: IEmployeeModel;
  editEmployeeForm: FormGroup;
  name: string;
  age: string;
  salary: string;
  image: string;
  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly store: Store<IAppState>,
    private readonly service: EmployeeService,
  ) {}

  ngOnInit() {
    this.editEmployeeForm = this.createFormGroup();

    if (this.activatedRoute.snapshot.data.empres !== false) {
      const allEmployees = this.activatedRoute.snapshot.data.empres;
      if (this.activatedRoute.snapshot.params.id) {
        this.singleEmployee = allEmployees.find((employee) => employee.id === Number(this.activatedRoute.snapshot.params.id));
        }
      this.name = this.singleEmployee.employee_name;
      this.age = this.singleEmployee.employee_age;
      this.salary = this.singleEmployee.employee_salary;
      this.image = this.singleEmployee.profile_image;
    }
  }

  createFormGroup() {
    return new FormGroup({
      name: new FormControl('', Validators.required),
      age: new FormControl('', Validators.required),
      salary: new FormControl('', Validators.required),
      image: new FormControl('')
    });
  }

  editEmployee() {
    const employee = {
      id: this.activatedRoute.snapshot.params.id,
      employee_name: this.editEmployeeForm.value.name,
      employee_age: this.editEmployeeForm.value.age,
      employee_salary: this.editEmployeeForm.value.salary,
      profile_image: this.editEmployeeForm.value.image
    };

    const response = this.service.updateEmployee(Number(this.activatedRoute.snapshot.params.id), employee);
    response.subscribe(result => console.log(result));
    this.store.dispatch(new UpdateEmployee(employee, Number(this.activatedRoute.snapshot.params.id)));
  }
}
