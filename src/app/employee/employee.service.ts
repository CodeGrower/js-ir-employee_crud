import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { environment } from '../../environments/environment';
import { IEmployeeModel } from '../store/state/employee.state';

@Injectable({
  providedIn: 'root'
})

export class EmployeeService {

  constructor(
    private readonly http: HttpClient
  ) {}


  getAllEmployees(): Observable<IEmployeeModel[]> {
    return this.http.get<any>('http://dummy.restapiexample.com/api/v1/employees');
  }

  getEmployee(employeeID: number): Observable<IEmployeeModel> {
    return this.http.get<any>(`https://jsonplaceholder.typicode.com/${employeeID}`);
  }

  createEmployee(employee: IEmployeeModel) {
    return this.http.post<any>(`${environment.baseUrl}`, employee);
  }

  updateEmployee(employeeID: number, employee: IEmployeeModel) {
    return this.http.put<any>(`https://jsonplaceholder.typicode.com/${employeeID}`, employee);
  }

  deleteEmployee(employeeID: number) {
    return this.http.delete<any>(`${environment.baseUrl}${employeeID}`);
  }
}
