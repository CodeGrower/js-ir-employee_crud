import { NgModule } from '@angular/core';

import { ElistComponent } from './list/list.component';
import { EmployeeComponent } from './employee/employee.component';
import { EmployeeRoutingModule } from './employee.routing.module';
import { CommonModule } from '@angular/common';
import { EditComponent } from './edit/edit.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    ElistComponent,
    EmployeeComponent,
    EditComponent
  ],
  imports: [
    EmployeeRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [],
  exports: [
    ElistComponent,
    EmployeeComponent,
    EditComponent
  ]
})
export class EmployeeModule { }
