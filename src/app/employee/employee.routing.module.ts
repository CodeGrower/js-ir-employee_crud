import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { EmployeeComponent } from './employee/employee.component';
import { ElistComponent } from './list/list.component';
import { EmployeesResolver } from 'src/common/resolvers/employees-resolver.service';
import { EditComponent } from './edit/edit.component';

export const routes: Routes = [
  {
    path: 'employees',
    component: ElistComponent,
    resolve: {
      empres: EmployeesResolver
    }
  },
  {
    path: 'employees/:id',
    component: EmployeeComponent,
    resolve: {
      empres: EmployeesResolver
    }
  },
  {
    path: 'employees/edit/:id',
    component: EditComponent,
    resolve: {
      empres: EmployeesResolver
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [EmployeesResolver], // resolvers here
})
export class EmployeeRoutingModule {}
