import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IEmployeeModel } from 'src/app/store/state/employee.state';

@Component({
  selector: 'app-elist',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ElistComponent implements OnInit {
  allEmployees: Array<IEmployeeModel>;
  constructor(
    private readonly activatedRoute: ActivatedRoute,
  ) {
  }

  ngOnInit() {
    if (this.activatedRoute.snapshot.data.empres !== false) {
      this.allEmployees = Object.values(this.activatedRoute.snapshot.data.empres);
      console.log(this.allEmployees);
    }
  }
}
