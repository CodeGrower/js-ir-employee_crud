import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { IEmployeeModel } from 'src/app/store/state/employee.state';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})
export class EmployeeComponent implements OnChanges, OnInit {
  @Input() singleEmployee: IEmployeeModel;
  name: string;
  age: string;
  salary: string;
  image: string;
  id: string;
  showSingle = false;
  initials: string[];

  constructor(
    private readonly activatedRoute: ActivatedRoute,
  ) {}

  ngOnChanges() {
    if (this.singleEmployee) {
      console.log(this.singleEmployee)
    // singleEmployee is array of employee objects; correct here and in list component and edit component; new baseurl in service
      this.id = this.singleEmployee.id;
      this.setInfo();
    }
  }

  ngOnInit() {
    if (this.activatedRoute.snapshot.data.empres !== false) {
      const allEmployees = this.activatedRoute.snapshot.data.empres;
      if (this.activatedRoute.snapshot.params.id) {
        this.singleEmployee = allEmployees.find((employee) => employee.id === Number(this.activatedRoute.snapshot.params.id));
        if (this.singleEmployee) {
          this.showSingle = true;
          this.setInfo();
        }
      }
    }
  }

  setInfo() {
    this.name = this.singleEmployee.employee_name;
    this.age = this.singleEmployee.employee_age;
    this.salary = this.singleEmployee.employee_salary;
    this.image = this.singleEmployee.profile_image;
    // if (this.singleEmployee.profile_image.length > 0) {
    //   this.image = this.singleEmployee.profile_image;
    // } else {
    this.initials = this.singleEmployee.employee_name.split(' ');
    this.image = this.initials[0][0] + this.initials[1][0];
    // }
  }
}
