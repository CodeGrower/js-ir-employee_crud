import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { AppComponent } from './app.component';
import { EmployeeEffects } from './store/effects/employee.effects';
import { appReducers } from './store/reducers/app.reducer';
import { EmployeeModule } from './employee/employee.module';
import { routes, AppRoutingModule } from './app-routing.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    EffectsModule.forRoot([EmployeeEffects]),
    StoreModule.forRoot(appReducers),
    StoreDevtoolsModule.instrument(),
    HttpClientModule,
    AppRoutingModule,
    EmployeeModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
